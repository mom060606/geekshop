# -*- coding: utf-8 -*-
# @Author: MOM
# @Date:   2018-09-11 00:24:28
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-23 16:53:06
from django.urls import path

from .views import (BasketDetailView, ContactsListView, OrderRedirectView,
                    PaymentInvoiceView, ProductByCategoryListView,
                    ProductCreateView, ProductDeleteView, ProductDetailView,
                    ProductListView, ProductUpdateView,
                    ProductWithCategoryListView, SelfOrderCreate,
                    SelfOrderDelete, SelfOrderList, SelfOrderRead,
                    SelfOrderSTPRedirectView, SelfOrderUpdate,
                    createPictureView, deletePictureView, serchView)

app_name = 'main'
urlpatterns = [
    path('order/<int:pk>/', OrderRedirectView.as_view(), name='order'),
    path('payment/', PaymentInvoiceView.as_view(), name='payment'),
    path('products/<int:slug>/edit/', ProductUpdateView.as_view(), name='products_update'),
    path('products/<int:slug>/del/', ProductDeleteView.as_view(), name='products_delete'),
    path('products/add/', ProductCreateView.as_view(), name='products_add'),
    path('products/<int:slug>/', ProductDetailView.as_view(), name='products_detail'),
    path('products/category/<int:category>/', ProductByCategoryListView.as_view(), name='products_category'),
    path('createpicture/<int:related_obj>', createPictureView.as_view(), name='createPicture'),
    path('deletepicture/<int:pk>/', deletePictureView.as_view(), name='deletePicture'),
    path('serch/products/', serchView.as_view(), name='serch'),
    path('products/', ProductWithCategoryListView.as_view(), name='products'),
    path('contacts/', ContactsListView.as_view(), name='contacts'),
    path('basket/', BasketDetailView.as_view(), name='basket'),

    path('create_selforder/', SelfOrderCreate.as_view(), name='self_order_create'),
    path('delete_selforder/<int:pk>/', SelfOrderDelete.as_view(), name='self_order_delete'),
    path('read_selforder/<int:pk>/', SelfOrderRead.as_view(), name='self_order_read'),
    path('update_selforder/<int:pk>/', SelfOrderUpdate.as_view(), name='self_order_update'),
    path('list_selforder/', SelfOrderList.as_view(), name='self_order_list'),
    path('toproc_selforder/<int:pk>/', SelfOrderSTPRedirectView.as_view(), name='self_order_to_proc'),

    path('', ProductListView.as_view(), name='home'),
]
