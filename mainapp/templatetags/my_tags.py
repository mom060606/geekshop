# -*- coding: utf-8 -*-
# @Author: MOM
# @Date:   2018-10-03 22:48:18
# @Last Modified by:   MOM
# @Last Modified time: 2018-10-03 23:21:12
from django import template

register = template.Library()


@register.simple_tag
def tf(p):
    return round(float(p), 2)


@register.filter
def dot_end(s):
    if type(s) != str:
        return s
    if s.endwith('.'):
        return s
    else:
        return s + '.'
