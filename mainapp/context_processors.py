# -*- coding: utf-8 -*-
# @Author: MOM
# @Date:   2018-09-25 22:03:02
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-23 11:17:36
import os

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

SU = settings.STATIC_URL
SD = settings.STATICFILES_DIRS


def curr_path(request):
    return {'curr_path_name': request.resolver_match.view_name}


def main_menu(request):
    main_menu = [
        {'get_url': 'main:home', 'title': _('HOME')},
        {'get_url': 'main:products', 'title': _('PRODUCTS')},
        # {
        #     'get_url': '#',
        #     'title': _('HISTORY')
        # }, {
        #     'get_url': '#',
        #     'title': _('SHOWROOM')
        # },
        {'get_url': 'main:contacts', 'title': _('CONTACT')},
        {'get_url': 'about', 'title': _('ABOUT US')},
    ]
    if request.user.is_authenticated:
        main_menu += [{
            'get_url': 'acc:profile',
            'title': _(f'PROFILE {request.user.first_name}'),
            'submenu': [{
                'get_url': 'acc:profile',
                'title': _(f'PROFILE {request.user.first_name}')
            }, {
                'get_url': 'main:self_order_list',
                'title': _('LIST SELF ORDER')
            }]
        }]
        main_menu += [{'get_url': 'acc:logout', 'title': _('LOG OUT')}]
    else:
        main_menu += [{'get_url': 'acc:login', 'title': _('LOG IN')}, {'get_url': 'acc:singup', 'title': _('Sign Up')}]
    return {'main_menu': main_menu, 'VERSION': settings.VERSION}


def themes(request):
    """
    Returns context variables containing information about theme.
    """
    if request.user.is_authenticated and 'admin' not in request.build_absolute_uri():
        sd = f'\\{request.user.user_theme.title}' if request.user.user_theme.title not in settings.STATICFILES_DIRS else ''
        s = f'{request.user.user_theme.title}/' if request.user.user_theme.title not in settings.STATIC_URL else ''
        settings.STATIC_URL = settings.STATIC_URL + s
        settings.STATICFILES_DIRS = (os.path.join(settings.BASE_DIR, 'static' + sd),)
    else:
        settings.STATIC_URL = SU
        settings.STATICFILES_DIRS = SD
    return {}
