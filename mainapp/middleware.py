# -*- coding: utf-8 -*-
# @Author: maximus
# @Date:   2018-10-05 18:36:36
# @Last Modified by:   MaxST
# @Last Modified time: 2019-01-27 13:00:15
from threading import current_thread

from django.utils.deprecation import MiddlewareMixin

_locals = {}


class RequestThemeMiddleware(MiddlewareMixin):
    @property
    def ident(self):
        return current_thread().ident

    @property
    def locals(self):
        _locals[self.ident] = _locals.get(self.ident, {})
        return _locals[self.ident]

    def clear_locals_dict(self):
        _locals.pop(self.ident, None)

    def process_request(self, request):
        self.locals['request'] = request

    def process_response(self, request, response):
        self.clear_locals_dict()
        return response

    def process_exception(self, request, exception):
        self.clear_locals_dict()

    @staticmethod
    def get_request():
        return _locals.get(current_thread().ident, {}).get('request')


class DisableCSRF(MiddlewareMixin):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
