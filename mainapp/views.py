from django.contrib.admin.models import ADDITION, LogEntry
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.contrib.messages.views import SuccessMessageMixin
from django.core.cache import cache
from django.db import transaction
from django.db.models import Count
from django.forms import inlineformset_factory, modelform_factory
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView)
from django.views.generic.base import ContextMixin
from django.views.generic.edit import FormView

from .models import (Contacts, Invoice, Picture, Product, ProductCategory,
                     Specification)
from .simple_search import search_form_factory

User = get_user_model()
VIEW = 4


class MenuOver(ContextMixin):
    """Добавляем параметр для наложения меню на первый элемент

    Extends:
        ContextMixin
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu_over'] = 'menu-over'
        return context


class ProductListView(ListView, MenuOver):
    """
    Основная страница

    **Context**

    ``object_list``
        Набор продуктов :model:`mainapp.Product`
    ``curr_path_name``
        Имя текущей ссылки. Для доп. класса меню.
    ``menu_over``
        Доп. класс для тега main

    **Template:**

    :template:`mainapp/product_list.html`
    """

    model = Product

    @cached_property
    def con_type_prod(self):
        return ContentType.objects.get_for_model(Product).pk

    def get_queryset(self):
        query = super().get_queryset()
        return query.prefetch_related('pictures')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET.get('new_list', None):
            context['new_list'] = 1
            logs = LogEntry.objects.filter(content_type_id=self.con_type_prod, action_flag=ADDITION).values('object_id').order_by('-action_time')
            tab_content = [Product.objects.filter(pk=int(l['object_id'])).first() for l in logs if Product.objects.filter(pk=int(l['object_id'])).first()][:4]
        else:
            tab_content = self.object_list.prefetch_related('pictures', 'pricelist')
        context['tab_content'] = tab_content
        views = LogEntry.objects.filter(
            content_type_id=self.con_type_prod, action_flag=VIEW).values('object_id').annotate(cn=Count('*')).order_by('-cn')
        popular = Product.objects.filter(pk__in=[v['object_id'] for v in views]).select_related('category').extra(
            select={
                'val':
                "SELECT COUNT(*) as cn FROM django_admin_log WHERE content_type_id = %s AND action_flag = %s AND object_id = mainapp_product.core_ptr_id GROUP BY object_id"
            },
            select_params=(self.con_type_prod, VIEW),
        ).order_by('-val')
        context['popular_list'] = popular.prefetch_related('pictures')
        return context

    def post(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('main:home'))


class CategoryMixin(ContextMixin):

    @cached_property
    def get_category(self):
        key = 'category'
        category = cache.get(key)
        if not category:
            category = ProductCategory.objects.all()
            cache.set(key, category)
        return category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.get_category
        return context


class ProductWithCategoryListView(ListView, MenuOver, CategoryMixin):
    """
    Отображение станицы списка Продуктов

    **Context**

    ``object_list``
        Набор продуктов :model:`mainapp.Product`
    ``category``
        Набор всех категорий :model:`mainapp.ProductCategory` для построения подменю

    **Template:**

    :template:`mainapp/products.html`
    """
    model = Product
    paginate_by = 3
    template_name = "mainapp/products.html"


class ProductByCategoryListView(ProductWithCategoryListView):
    """
    Отображение товара :model:`mainapp.Product` по категории :model:`mainapp.ProductCategory`

    **Context**

    ``object_list``
        Набор продуктов :model:`mainapp.Product` по переданной категории :model:`mainapp.ProductCategory`
    ``category``
        Набор всех категорий :model:`mainapp.ProductCategory` для построения подменю
    """

    def get_queryset(self):
        return Product.objects.filter(category__id=self.kwargs['category'])


class ProductDetailView(CategoryMixin, DetailView):
    """
    Детализация продукта

    **Context**

    ``object``
        Инстанс текщего продукта :model:`mainapp.Product`
    ``category``
        Набор всех категорий :model:`mainapp.ProductCategory` для построения подменю
    ``price``
        Розничная цена. Получаем из :model:`mainapp.PriceList` где type_price=6
    ``related_products``
        Продукты с тойже категорией

    **Template:**

    :template:`mainapp/product_detail.html`
    """
    model = Product
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_products'] = Product.objects.filter(category=self.object.category).exclude(id=self.object.id)
        price = self.object.pricelist.filter(type_price=6).first()
        context['price'] = str(price.price) if price else '0'

        user = self.request.user.pk if self.request.user.is_authenticated else 56
        LogEntry.objects.log_action(
            user_id=user,
            content_type_id=ContentType.objects.get_for_model(self.object).pk,
            object_id=self.object.pk,
            object_repr=str(self.object),
            action_flag=VIEW,
            change_message="Accessed object %s in %s." % (str(self.object), ContentType.objects.get_for_model(self.object)))
        return context


class ContactsListView(ListView, MenuOver):
    """
    Отображение контактов :model:`mainapp.Contacts`.
    """
    model = Contacts


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    """
    Обновление записи :model:`mainapp.Product`
    """
    model = Product
    slug_field = 'pk'
    fields = '__all__'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.success_url = reverse('main:products_detail', args=[self.object.id])
        return super().post(request, *args, **kwargs)


class ProductDeleteView(LoginRequiredMixin, DeleteView):
    """
    Удаление записи :model:`mainapp.Product`

    **Param**

    ``success_url``
        url('main:home')
    """
    model = Product
    slug_field = 'pk'

    def get_success_url(self):
        return reverse_lazy('main:home')


class ProductCreateView(LoginRequiredMixin, CreateView):
    """
    Создание продукта :model:`mainapp.Product`

    **Param**

    ``success_url``
        '/products/{id}/'

    **Template:**

    :template:`mainapp/add-product.html`
    """
    model = Product
    fields = '__all__'
    success_url = '/products/{id}/'


class BasketDetailView(LoginRequiredMixin, SuccessMessageMixin, DetailView, MenuOver):
    """
    Отображение содержимого корзины :model:`mainapp.Invoice`
    """
    model = Invoice

    def get_object(self, queryset=None):
        return self.request.user.invoices.get_last_or_create(owner=self.request.user, status=self.model.FORMING)

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        up, down, delete = request.POST.get('up', None), request.POST.get('down', None), request.POST.get('del', None)
        spc = obj.spc.filter(id=up or down or delete)
        if delete:
            spc.hard_delete()
        elif up != down:
            i = 1 if up else -1
            for s in spc:
                q = i if s.quant + i > 0 else i + 1
                if s.ware.whcard.rest_quant() < q:
                    q = 0
                s.quant += q
                try:
                    s.save()
                except ValueError:
                    return HttpResponseRedirect(reverse_lazy('main:basket'))
        if obj.total_cost == 0:
            obj.delete()
        return HttpResponseRedirect(reverse_lazy('main:basket'))


class OrderRedirectView(LoginRequiredMixin, RedirectView):
    """
    Заказ товара post/put

    redirect to url('main:basket')
    """

    def get_redirect_url(self, *args, **kwargs):
        inv = self.request.user.invoices.get_last_or_create(owner=self.request.user, status=Invoice.FORMING)
        price = self.request.POST.get('price') or '0'
        spc, created = inv.spc.get_or_create(ware_id=kwargs['pk'], price=str(float(price.replace(',', '.'))))
        spc.quant += 1
        spc.save()
        return reverse('main:basket')


class createPictureView(LoginRequiredMixin, CreateView):
    """
    Добавить картинку к товару

    Extends:
        LoginRequiredMixin
        CreateView

    Variables:
        fields {tuple} -- [description]
        model {[type]} -- [description]
    """
    fields = ('image', 'title')
    model = Picture

    def form_valid(self, form):
        form.instance.related_obj_id = self.kwargs.get('related_obj')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('main:products_detail', args=(self.object.related_obj.id, ))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_obj'] = self.kwargs.get('related_obj')
        return context


class deletePictureView(DeleteView):
    """
    Удалить картинку товара

    Extends:
        DeleteView

    Variables:
        model {[type]} -- [description]
        template_name {str} -- [description]
    """
    model = Picture
    template_name = 'mainapp/product_confirm_delete.html'

    def get_success_url(self):
        return reverse('main:products_detail', args=(self.object.related_obj.id, ))


class PaymentInvoiceView(BasketDetailView):
    """Оплата"""
    success_message = _('Оплата была выполнена')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('main:basket'))

    def post(self, request, *args, **kwargs):
        if self.request.is_ajax():
            obj = self.get_object()
            if 'pay' in request.POST:
                obj.status = self.model.PAID
                obj.save()
                return HttpResponseRedirect(reverse_lazy('main:basket'))
        return super().post(request, *args, **kwargs)


class serchView(FormView, ListView):
    model = Product
    success_url = reverse_lazy('main:serch')
    form_class = search_form_factory(model.objects.all(), ['title', 'description'])
    template_name = 'mainapp/serch.html'

    def get_queryset(self):
        self.form = self.get_form()
        if self.form.is_valid():
            return self.form.get_queryset()
        return super().get_queryset()

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class SelfOrderList(LoginRequiredMixin, ListView):
    model = Invoice

    def get_queryset(self):
        return self.request.user.invoices.all()


class SelfOrderDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Invoice
    success_url = reverse_lazy('main:self_order_list')
    success_message = _('Deleted Successfully')

    def get_queryset(self):
        return self.request.user.invoices.all()


class SelfOrderRead(LoginRequiredMixin, DetailView):
    model = Invoice
    template_name = 'mainapp/order_detail.html'

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        response.update({'isread': 1})
        return response


class SelfOrderCreate(LoginRequiredMixin, CreateView):
    model = Invoice
    fields = []
    success_url = reverse_lazy('main:self_order_list')
    success_message = _('Created Successfully')

    def get_form_class(self):
        response = super().get_form_class()
        self.formset_class = inlineformset_factory(self.model, Specification, form=modelform_factory(Specification, fields=('ware', 'quant')), extra=1, fk_name='invioce')
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = self.formset_class
        return context

    def form_valid(self, form):
        self.formset = self.formset_class(self.request.POST)
        if self.formset.is_valid():
            form.instance.owner = self.request.user
            self.formset.instance = self.object = form.save()
            self.formset.save()
            if self.object.total_cost == 0:
                self.object.delete()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.form_invalid(form)


class SelfOrderUpdate(LoginRequiredMixin, UpdateView):
    model = Invoice
    fields = []
    template_name = 'mainapp/invoice_form.html'
    success_url = reverse_lazy('main:self_order_list')

    def get_form_class(self):
        response = super().get_form_class()
        self.formset_class = inlineformset_factory(self.model, Specification, form=modelform_factory(Specification, fields=('ware', 'quant')), extra=1, fk_name='invioce')
        return response

    def get_form(self, form_class=None):
        response = super().get_form(form_class=form_class)
        self.formset = self.formset_class(instance=self.object, data=response.data or None)
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = self.formset
        return context

    def get_queryset(self):
        return self.request.user.invoices.all()

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            if self.formset.is_valid():
                self.formset.save()
            else:
                return self.form_invalid(form)
        # удаляем пустой заказ
        if self.object.total_cost == 0:
            self.object.delete()
        return HttpResponseRedirect(self.get_success_url())


class SelfOrderSTPRedirectView(LoginRequiredMixin, RedirectView):
    """
    Отправка товара в обработку

    """

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs.get('pk')
        if pk:
            inv = self.request.user.invoices.get(id=pk)
            inv.status = Invoice.SENT_TO_PROCEED
            inv.save()
        return reverse('main:self_order_list')
