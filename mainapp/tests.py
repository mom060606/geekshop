from accounts.models import Theme, User
from django.core.management import call_command
from django.test import TestCase

from .models import Product, ProductCategory, WhCard


class TestUrls(TestCase):
    """docstring for TestIndex"""

    def setUp(self):
        theme = Theme.objects.create(title='theme0', active=True)
        self.superuser = User.objects.create_superuser('django2', 'django2@geekshop.local', 'geekbrains', user_theme=theme, is_staff=True, active=True)
        self.user = User.objects.create_user('tarantino', 'tarantino@geekshop.local', 'geekbrains', user_theme=theme, is_staff=True, active=True)
        call_command('loaddata', 'fixtures/flatpage.json')
        self.ware = Product.objects.get(id=10)
        self.ost = WhCard.objects.create(ware=self.ware, quant=99, price=35.10)

    def simple_test_url(self, url):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_mainapp_url(self):

        # без логина
        self.simple_test_url('/')
        self.simple_test_url('/contacts/')
        self.simple_test_url('/about/')
        self.simple_test_url('/serch/products/')
        response = self.client.get('/basket/')
        self.assertRedirects(response, '/auth/login/?next=/basket/')

        for category in ProductCategory.objects.all():
            self.simple_test_url(f'/products/category/{category.id}/')

        all_prod = Product.objects.all()
        for prod in all_prod:
            self.simple_test_url(f'/products/{prod.id}/')

        # С логином
        self.client.login(username='django2', password='geekbrains')
        response = self.client.get('/')
        self.assertContains(response, 'PROFILE', status_code=200)
        self.assertEqual(response.context['user'].is_superuser, True)
        self.simple_test_url('/basket/')
        self.simple_test_url('/create_selforder/')
        self.simple_test_url('/products/add/')

        for prod in all_prod:
            self.simple_test_url(f'/products/{prod.id}/edit/')

        response = self.client.post('/order/10/', data={'price': 36.7})
        self.assertRedirects(response, '/basket/')
        self.simple_test_url('/list_selforder/')

        inv = self.superuser.invoices.first()
        self.assertEqual(len(inv.spc.all()), 1)

        response = self.client.get(f'/read_selforder/{inv.id}/')
        self.assertContains(response, 'статус: Формируется', status_code=200)

        self.simple_test_url(f'/update_selforder/{inv.id}/')

        response = self.client.get(f'/toproc_selforder/{inv.id}/')
        self.assertRedirects(response, '/list_selforder/')

        response = self.client.get(f'/read_selforder/{inv.id}/')
        self.assertContains(response, 'статус: Отправлен в обработку', status_code=200)

        response = self.client.post('/order/10/', data={'price': 36.7})
        self.assertRedirects(response, '/basket/')
        response = self.client.post('/order/10/', data={'price': 36.7})
        self.assertRedirects(response, '/basket/')
        inv = self.superuser.invoices.all()[1]
        self.assertEqual(len(inv.spc.all()), 1)
        self.assertEqual(inv.total_quant, 2)

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'accounts')


class TestProductCategory(TestCase):

    def setUp(self):
        theme = Theme.objects.create(title='theme0', active=True)
        self.superuser = User.objects.create_superuser('django2', 'django2@geekshop.local', 'geekbrains', user_theme=theme, is_staff=True, active=True)
        self.user = User.objects.create_user('tarantino', 'tarantino@geekshop.local', 'geekbrains', user_theme=theme, is_staff=True, active=True)
        call_command('loaddata', 'fixtures/flatpage.json')
        self.ware = Product.objects.get(id=10)
        self.ost = WhCard.objects.create(ware=self.ware, quant=99, price=35.10)
        ProductCategory.objects.create(title='For delete')

    def _check_active(self, obj, val):
        obj.active = val
        obj.save()
        for p in obj.product_set.all():
            self.assertEqual(p.active, obj.active)

    def test_save(self):
        cat = ProductCategory.objects.last()
        self._check_active(cat, False)
        self._check_active(cat, True)

    def test_str(self):
        cat = ProductCategory.objects.first()
        self.assertEqual(cat.title, str(cat))

    def test_delete(self):
        cat = ProductCategory.objects.first()
        self.assertTrue(cat.active)
        pk = cat.id
        cat.delete()
        q_del = ProductCategory.objects.filter(pk=pk)
        self.assertEqual(len(q_del), 0)
        cat_del = ProductCategory.all_objects.get(pk=pk)
        self.assertFalse(cat_del.active)

    def test_hard_delete(self):
        cat = ProductCategory.all_objects.get(title='For delete')
        cat.hard_delete()
        q = ProductCategory.all_objects.filter(title='For delete')
        self.assertEqual(len(q), 0)

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'accounts')
