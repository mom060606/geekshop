from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import (Contacts, DetailSpecification, Invoice, MakerProduct,
                     Picture, PriceList, Product, ProductCategory,
                     Specification, TypePriceList, WhCard)


class EditLinkToInlineObject(object):
    readonly_fields = ('edit_link', )

    def edit_link(self, instance):
        url = reverse('admin:%s_%s_change' % (
            instance._meta.app_label,  instance._meta.model_name),  args=[instance.pk] )
        return mark_safe(u'<a href="{u}">edit</a>'.format(u=url)) if instance.pk else ''


class HardDeleteMixin(object):
    def get_queryset(self, request):
        qs = self.model.all_objects.get_queryset()
        return qs


class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = 'related_obj'
    fields = ('title', 'active', 'image')


class PriceInline(admin.TabularInline):
    model = PriceList
    fk_name = 'product'
    fields = ('type_price', 'price')


class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    inlines = (PictureInline, )
    list_editable = ('sort', )


admin.site.register(ProductCategory, ProductCategoryAdmin)


class PictureAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    fields = ('title', 'active', 'image', 'sort', 'description')


admin.site.register(Picture, PictureAdmin)


class ProductAdmin(HardDeleteMixin, admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    inlines = (
        PictureInline,
        PriceInline,
    )
    exclude = ('pictures', )
    list_editable = ('sort', )


admin.site.register(Product, ProductAdmin)


class MakerProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'active', 'sort')
    list_editable = ('sort', )


admin.site.register(MakerProduct, MakerProductAdmin)


class ContactsAdmin(admin.ModelAdmin):
    list_display = ('title', 'phone_number', 'email', 'addres')


admin.site.register(Contacts, ContactsAdmin)


class TypePriceListAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    list_editable = ('sort', )


admin.site.register(TypePriceList, TypePriceListAdmin)


class PriceListAdmin(admin.ModelAdmin):
    list_display = (
        'product',
        'type_price',
        'price',
    )


admin.site.register(PriceList, PriceListAdmin)


class SpecificationInline(EditLinkToInlineObject, admin.TabularInline):
    model = Specification
    fk_name = 'invioce'
    fields = ('ware', 'quant', 'price', 'cost', 'edit_link')
    readonly_fields = ('cost', 'edit_link',)


class DetailSpecificationInline(EditLinkToInlineObject, admin.TabularInline):
    model = DetailSpecification
    fk_name = 'specification'
    fields = ('specification', 'whcard', 'quant', 'edit_link')


class InvoiceAdmin(HardDeleteMixin, admin.ModelAdmin):
    list_display = (
        'owner',
        'date',
        'status',
        'active',
    )
    list_editable = ('status', )
    inlines = (SpecificationInline, )


class SpecificationAdmin(admin.ModelAdmin):
    '''
        Admin View for Specification
    '''
    list_display = ('invioce', 'ware', 'quant', 'price', 'cost')
    list_filter = ('invioce',)
    readonly_fields = ('cost',)
    inlines = [
        DetailSpecificationInline,
    ]

    def get_queryset(self, request):
        qs = self.model.all_objects.get_queryset()
        return qs


admin.site.register(DetailSpecification)
admin.site.register(Specification, SpecificationAdmin)
admin.site.register(Invoice, InvoiceAdmin)


class WhCardAdmin(admin.ModelAdmin):
    '''
        Admin View for WhCard
    '''
    list_display = ('ware', 'date_reg', 'quant', 'price',)
    list_filter = ('ware',)


admin.site.register(WhCard, WhCardAdmin)
