# Generated by Django 2.1.1 on 2018-09-19 21:12

import os

from django.core import serializers
from django.db import migrations

fixture_dir = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../../fixtures'))
maodel_fixture = [
    'productcategory', 'typepricelist', 'makerproduct', 'product', 'pricelist',
    'picture', 'contacts',
    # 'flatpage'
]
fixture_file = [f'{i}.json' for i in maodel_fixture]


def myDataMigrate(apps, schema_editor):
    """
    Load default Data
    """
    for fixt_filen in fixture_file:
        fixture = open(os.path.join(fixture_dir, fixt_filen), 'rb')
        objects = serializers.deserialize('json', fixture)
        for obj in objects:
            new_obj = type(obj.object)()
            new_obj.__dict__ = vars(obj.object)
            new_obj.save()


def unload_fixture(apps, schema_editor):
    "Brutally deleting all entries for this model..."
    for my_model in maodel_fixture[::-1]:
        MyModel = apps.get_model("mainapp", my_model)
        MyModel.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0003_product_maker'),
    ]

    operations = [
        migrations.RunPython(myDataMigrate, reverse_code=unload_fixture)
    ]
