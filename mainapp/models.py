from datetime import timedelta

from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.db.models import F, Sum
from django.db.models.query import QuerySet
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _


class CoreQuerySet(QuerySet):
    def delete(self):
        return super().update(active=False)

    def hard_delete(self):
        return super().delete()


class CoreManager(models.Manager):
    def get_queryset(self):
        return CoreQuerySet(self.model).filter(active=True).order_by('sort')

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class Core(models.Model):
    """Ядро для моделей"""

    title = models.CharField(_('title'), max_length=250, default='', blank=True, null=False)
    description = models.TextField(_('description'), null=True, blank=True, default='')
    sort = models.IntegerField(_(u'sort'), default=0, blank=True, null=True)
    active = models.BooleanField(_(u'active'), default=True, db_index=True)

    all_objects = models.Manager()
    objects = CoreManager()

    def __str__(self):
        return f'{self.title}'

    def delete(self):
        self.active = False
        self.save()

    def hard_delete(self):
        super().delete()


class Picture(Core):
    """Картинки для объектов"""

    class Meta:
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image = models.ImageField(upload_to='images')
    related_obj = models.ForeignKey(Core, verbose_name=_('pictures'), null=True, blank=True, related_name='pictures', on_delete=models.CASCADE)


class ProductCategory(Core):
    """Категории товара"""

    class Meta:
        verbose_name = _('Продуктовая категория')
        verbose_name_plural = _('Продуктовые категории')
        ordering = ('sort',)

    def save(self, *args, **kwargs):
        if self.id:
            old = type(self).all_objects.get(pk=self.id)
            if old.active != self.active:
                self.product_set.update(active=self.active)
        super().save(*args, **kwargs)


class MakerProduct(Core):
    """Производитель продукции"""

    class Meta:
        verbose_name = _('Производитель')
        verbose_name_plural = _('Производители')
        ordering = ('sort',)


class Product(Core):
    """Товары/Продукты"""

    class Meta:
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')
        ordering = ('sort',)

    category = models.ForeignKey(ProductCategory, null=True, on_delete=models.SET_NULL)
    code = models.CharField(max_length=6, default='', blank=True, null=False)
    maker = models.ForeignKey(MakerProduct, null=True, on_delete=models.SET_NULL)

    @property
    def rest_quant(self):
        return self.whcard.rest_quant()


class Contacts(models.Model):
    """Контыкты"""

    class Meta:
        verbose_name = _('Контакт')
        verbose_name_plural = _('Контакты')

    title = models.CharField(_('title'), max_length=250, default='', blank=True, null=False)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{10,12}$', message="Phone number must be entered in the format:" + " '+79995554433'. Up to 12 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    email = models.EmailField(max_length=70, blank=True, null=True)
    addres = models.CharField(max_length=80)


class TypePriceList(Core):
    """Типы прайс-листов/цен"""

    class Meta:
        verbose_name = _('Тип прайс-листа')
        verbose_name_plural = _('Типы прайс-листов')
        ordering = ('sort',)


class PriceList(models.Model):
    """Цены для продукции"""

    class Meta:
        verbose_name = _('Прайс-лист')
        verbose_name_plural = _('Прайс-листы')

    product = models.ForeignKey(Product, verbose_name=_('pricelist'), related_name='pricelist', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    type_price = models.ForeignKey(TypePriceList, verbose_name=_('type_price'), related_name='type_price', on_delete=models.CASCADE)


class ManagerInvoice(CoreManager):
    def get_last_or_create(self, **kwargs):
        obj = self.get_queryset().filter(**kwargs).order_by('-date').first()
        if not obj:
            obj = self.create(**kwargs)
        return obj


class Invoice(Core):
    """Счета покупателей"""
    FORMING = _('FM')
    SENT_TO_PROCEED = _('STP')
    PROCEEDED = _('PRD')
    PAID = _('PD')
    READY = _('RDY')
    CANCEL = _('CNC')
    DONE = _('DN')

    ORDER_STATUS_CHOICES = (
        (FORMING, _('Формируется')),
        (SENT_TO_PROCEED, _('Отправлен в обработку')),
        (PAID, _('Оплачен')),
        (PROCEEDED, _('Обрабатывается')),
        (READY, _('Готов к выдаче')),
        (CANCEL, _('Отменен')),
        (DONE, _('Выполнен')),
    )

    FINISH_STATE = (DONE, CANCEL)

    class Meta:
        verbose_name = _('Счет')
        verbose_name_plural = _('Счета')

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, related_name='invoices', on_delete=models.PROTECT)
    date = models.DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True, blank=True)
    date_modif = models.DateTimeField(verbose_name=_('Дата модификации'), auto_now=True, blank=True)
    status = models.CharField(verbose_name='Статус', max_length=3, choices=ORDER_STATUS_CHOICES, default=FORMING, blank=True)
    specification = models.ManyToManyField(Product, through='Specification')

    objects = ManagerInvoice()

    @property
    def total_cost(self):
        return self.spc.total_cost()

    @property
    def total_quant(self):
        return self.spc.total_quant()

    def save(self, **kwargs):
        if self.status in self.FINISH_STATE:
            self.active = False
        super().save(**kwargs)
        if not self.title:
            date = self.date.strftime('%Y-%m-%d')
            self.title = f'№ {self.id} от {date}'
            self.save()


class SpcQuerySet(CoreQuerySet):
    def total_cost(self):
        return self.aggregate(summ=Sum(F('quant') * F('price'), output_field=models.FloatField()))['summ'] or 0.00

    def total_quant(self):
        return self.aggregate(summ=Sum(F('quant'), output_field=models.FloatField()))['summ'] or 0.00

    def hard_delete(self):
        for spc in self:
            spc.dspc.delete()
        return super().hard_delete()


class Specification(Core):
    """Спецификация Счета"""

    class Meta:
        verbose_name = _('Спецификация')
        verbose_name_plural = _('Спецификации')

    ware = models.ForeignKey(Product, null=False, on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quant = models.PositiveIntegerField(default=0, null=False)
    invioce = models.ForeignKey(Invoice, related_name='spc', null=False, on_delete=models.CASCADE)
    detail_specification = models.ManyToManyField('WhCard', through='DetailSpecification')

    objects = SpcQuerySet.as_manager()

    def __str__(self):
        return f'{self.ware} {self.quant} {self.price}'

    @property
    def cost(self):
        return f'{round(self.quant * self.price, 2)}'

    def make_detail(self):
        delta = self.quant - self.dspc.total_quant()
        if delta > 0:
            for card in WhCard.objects.get_card(self.ware, quant=delta):
                dspc, created = self.dspc.get_or_create(specification=self, whcard=WhCard.objects.get(id=card['id']))
                dspc.quant += card['quant']
                dspc.save()
        elif delta < 0:
            for dcard in reversed(self.dspc.all()):
                q = max([-dcard.quant, delta])
                dcard.quant += q
                dcard.save()
                if dcard.quant == 0:
                    dcard.delete()
                delta -= q
                if delta == 0:
                    break
        return True if self.quant == self.dspc.total_quant() else False

    def save(self, is_remains=True, **kwargs):
        """
        Arguments:
            **kwargs {dict} -- [description]
        """
        if self.price == 0 and len(self.ware.pricelist.all()) > 0:
            self.price = self.ware.pricelist.first().price
        with transaction.atomic():
            super().save(**kwargs)
            if is_remains:
                if not self.make_detail():
                    raise ValueError("save() Not meke detail for quant: %s" % self.quant)


class ModelManagerDetailSpc(QuerySet):

    def total_quant(self):
        return self.aggregate(summ=Sum(F('quant'), output_field=models.FloatField()))['summ'] or 0.00

    def delete(self):
        for obj in self:
            obj.delete()
        return super().delete()


class DetailSpecification(models.Model):
    """
    Расшифровка спецификации

    Предназначена для учета партий и доп. параметров спецификации

    Extends:
        models.Model

    Variables:
        specification {[type]} -- [description]
        quant {[type]} -- [description]
        whcard {[type]} -- [description]
    """
    class Meta:
        verbose_name = _('Расшифровка спецификации')
        verbose_name_plural = _('Расшифровки спецификации')

    specification = models.ForeignKey(Specification, related_name='dspc', null=False, on_delete=models.CASCADE)
    quant = models.PositiveIntegerField(default=0, null=False)
    whcard = models.ForeignKey('WhCard', related_name='dspc', on_delete=models.PROTECT)

    objects = ModelManagerDetailSpc.as_manager()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.old_quant = self.quant

    def __str__(self):
        return f'{self.specification.ware} quant:{self.quant} wh:{self.whcard.id}'

    def _change_wh_quant(self, quant):
        if self.whcard:
            self.whcard.quant += quant
            self.whcard.save()

    def delete(self):
        self._change_wh_quant(self.quant)
        super().delete()

    def save(self, **kwargs):
        self._change_wh_quant(self.old_quant - self.quant)
        super().save(**kwargs)


class WhCardQuerySet(QuerySet):
    def get_card(self, ware, date=None, quant=0):
        response = []
        date = date if date else now()
        if quant == 0:
            return self.filter(id=0)

        for card in self.filter(ware=ware, date_reg__lte=date, quant__gt=0).values('id', 'quant'):
            q = min([quant, card['quant']])
            response.append({'id': card['id'], 'quant': q})
            quant -= q
            if quant == 0:
                break
        return response

    def get_quant_rest(self, ware, date=now()):
        return self.filter(ware=ware, date_reg__lte=date, quant__gt=0).aggregate(summ=Sum(F('quant'), output_field=models.FloatField()))['summ'] or 0.00

    def rest_quant(self):
        return self.aggregate(summ=Sum(F('quant'), output_field=models.FloatField()))['summ'] or 0.00


class WhCard(models.Model):
    class Meta:
        verbose_name = _('Партия товара')
        verbose_name_plural = _('Партии товара')

    ware = models.ForeignKey(Product, verbose_name=_('Номенклатура'), null=False, related_name='whcard', on_delete=models.PROTECT)
    quant = models.PositiveIntegerField(verbose_name=_('Количество'), default=0, null=False)
    # type_quant = models.CharField()  # 'FreeRest'
    # part_info = models.ForeignKey(on_delete=models.CASCADE)
    date_reg = models.DateTimeField(verbose_name=_('Дата регистрации'), auto_now_add=True)
    price = models.DecimalField(verbose_name=_('Цена'), max_digits=10, decimal_places=2, default=0)

    objects = WhCardQuerySet.as_manager()

    def __str__(self):
        return f'{self.ware} q:{self.quant} p:{self.price}'
