# -*- coding: utf-8 -*-
# @Author: maximus
# @Date:   2018-10-04 18:36:22
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-16 10:30:51
import os
from mainapp.middleware import RequestThemeMiddleware
from django.conf import settings
from django.template.loaders.filesystem import Loader as FileSystemLoader


class Loader(FileSystemLoader):

    def get_dirs(self):
        request = RequestThemeMiddleware.get_request()
        theme = 'theme0'
        if request:
            user = getattr(request, 'user', None)
            if user and user.is_authenticated and user.user_theme and user.user_theme.title:
                theme = user.user_theme.title

        return [*[os.path.join(settings.BASE_DIR, f'templates/{theme}')], *super(Loader, self).get_dirs()]
