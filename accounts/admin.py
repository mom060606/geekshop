from django.contrib import admin
from django.contrib.auth.models import Group, Permission
from mainapp.admin import PictureInline

from .models import User, Theme


class ThemeAdmin(admin.ModelAdmin):
    list_display = ('title', )
    exclude = ('sort', 'description', )


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'email',
        'active',
        'user_theme',
    )
    inlines = (PictureInline, )
    exclude = ('password', 'sort', 'active', 'description', 'title',
               'last_login')


admin.site.register(User, UserAdmin)
admin.site.register(Theme, ThemeAdmin)

admin.site.unregister(Group)
admin.site.register(Group)
admin.site.register(Permission)
