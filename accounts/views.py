# -*- coding: utf-8 -*-
# @Author: Max ST
# @Date:   2018-12-09 18:03:40
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-16 10:31:30
import django.contrib.auth.views as dv
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, DeleteView, DetailView, UpdateView
from mainapp.views import MenuOver

from .forms import UserActivationRegisterForm, UserRegisterForm
from .models import Avatar, User


class UserNotAuthMixin(UserPassesTestMixin):
    url_redirect = '/'

    def test_func(self):
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        return HttpResponseRedirect(self.url_redirect)


class Login(UserNotAuthMixin, SuccessMessageMixin, dv.LoginView, MenuOver):
    """
    Авторизация пользователя

    Ввод логина и пароля

    Extends:
        SuccessMessageMixin
        LoginView
        MenuOver

    Variables:
        template_name {str} -- Имя шаблона
        success_message {str} -- Текст сообщения при удачном входе
    """

    template_name = 'accounts/login.html'
    success_message = _('Вход в систему выполнен')


class Logout(dv.LogoutView):
    """
    Выход пользователя

    [description]

    Extends:
        LogoutView

    Variables:
        success_message {str} -- Текст сообщения при удачном выходе
    """

    success_message = _('Вы вышли из системы')

    def get_next_page(self):
        next_page = super().get_next_page()
        if next_page:
            messages.success(self.request, self.success_message)
        return next_page


class SignUp(UserNotAuthMixin, SuccessMessageMixin, CreateView, dv.PasswordResetView):
    """
    Registration User with send email and post activation (SignUpConfirmView)
    """

    form_class = UserRegisterForm
    success_url = reverse_lazy('acc:login')
    url_redirect = reverse_lazy('acc:profile')
    title = _('Форма регистрации')
    template_name = 'accounts/user_signup_form.html'
    email_template_name = 'accounts/signup_email.html'
    subject_template_name = 'accounts/signup_subject.txt'
    success_message = _('Для активации аккаунта выслано письмо')


class SignUpConfirmView(dv.PasswordResetConfirmView):
    """
    Activation registration
    """

    success_url = reverse_lazy('acc:login')
    title = _('Подтверждение регистрации')
    template_name = 'accounts/signup_confirm.html'
    form_class = UserActivationRegisterForm
    post_reset_login = True
    post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'
    dv.INTERNAL_RESET_URL_TOKEN = 'set-active'


class Profile(LoginRequiredMixin, DetailView):
    """
    Профиль пользователя

    Extends:
        DetailView

    Variables:
        model {User} -- model Пользователь
    """

    model = User

    def get_object(self, queryset=None):
        return self.request.user


class changeProfile(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
    Изменени пользователя


    Extends:
        LoginRequiredMixin
        UpdateView

    Variables:
        fields {tuple} -- [description]
        form {[type]} -- [description]
        model {[type]} -- [description]
        success_url {str} -- [description]
    """

    fields = ('username', 'first_name', 'last_name', 'email', 'description', 'user_theme')
    form = UserCreationForm
    model = User
    success_url = '/'
    success_message = _('Профиль изменен')

    def get_object(self, queryset=None):
        return self.request.user


class createAvatarView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """docstring for DelView"""

    fields = ('image', 'title')
    model = Avatar
    success_message = _('Добавлен новый Аватар')

    def form_valid(self, form):
        form.instance.related_obj = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('acc:profile')


class deleteAvatarView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    """docstring for DelView"""

    model = Avatar

    def test_func(self):
        pk = self.kwargs.get('pk', 0)
        return self.request.user.pictures.filter(id=pk).exists()

    def get_success_url(self):
        return reverse_lazy('acc:profile')


class changePassword(LoginRequiredMixin, SuccessMessageMixin, dv.PasswordChangeView):
    template_name = 'accounts/change_pass.html'
    success_message = _('Пароль изменен')

    def get_success_url(self):
        return reverse_lazy('acc:profile')


class passwordResetView(dv.PasswordResetView):
    template_name = 'accounts/password_reset_form.html'
    email_template_name = 'accounts/password_reset_email.html'

    def post(self, request, *args, **kwargs):
        return super().post(request, args, kwargs)


class passwordResetConfirmView(dv.PasswordResetConfirmView):
    template_name = 'accounts/password_reset_confirm.html'
    success_url = reverse_lazy('acc:password_reset_complete')
    post_reset_login = True
    post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'


class passwordResetCompleteView(dv.PasswordResetCompleteView):
    template_name = 'registration/password_reset_complete.html'
