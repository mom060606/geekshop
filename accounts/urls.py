# -*- coding: utf-8 -*-
# @Author: MOM
# @Date:   2018-09-26 20:55:31
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-15 15:11:01
from django.urls import path

from .views import (Login, Logout, Profile, SignUp, SignUpConfirmView,
                    changePassword, changeProfile, createAvatarView,
                    deleteAvatarView)

app_name = 'acc'

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
    path('singup/', SignUp.as_view(), name='singup'),
    path('confirm/<uidb64>/<token>/', SignUpConfirmView.as_view(), name='signup_confirm'),
    path('profile/', Profile.as_view(), name='profile'),
    path('editprofile/', changeProfile.as_view(), name='changeProfile'),
    path('editpass/', changePassword.as_view(), name='changePass'),
    path('createavatar/<int:related_obj>', createAvatarView.as_view(), name='createAvatar'),
    path('deleteavatar/<int:pk>/', deleteAvatarView.as_view(), name='deleteAvatar'),
]
