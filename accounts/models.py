# -*- coding: utf-8 -*-
# @Author: Max ST
# @Date:   2018-12-09 18:13:14
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-16 17:05:44
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from mainapp.models import Core, Picture


class Theme(Core):
    class Meta:
        verbose_name = _("Тема пользователя")
        verbose_name_plural = _("Темы пользователя")


class User(AbstractUser, Core):
    """
    Пользователи

    Тут они хранятся

    ``Extends``
        :model:`auth.AbstractUser`
        :model:`mainapp.Core`
    """

    user_theme = models.ForeignKey(Theme, null=True, default=90, on_delete=models.SET_NULL, related_name="theme")
    language = models.CharField(_('Language'), max_length=5, blank=True)
    link = models.CharField(_('Link'), max_length=50, blank=True)

    class Meta:
        verbose_name = _("Пользователь")
        verbose_name_plural = _("Пользователи")


class Avatar(Picture):
    """
    Прокси для :model:`mainapp.Picture`

    ``Extends``
        :model:`mainapp.Picture`
    """

    class Meta:
        verbose_name = _("Аватар")
        verbose_name_plural = _("Аватары")
        proxy = True
