# -*- coding: utf-8 -*-
# @Author: Max ST
# @Date:   2018-12-16 16:50:43
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-16 17:58:08
from social_core.exceptions import AuthForbidden


def save_user_g(backend, user, response, *args, **kwargs):
    if backend.name == "google-oauth2":
        user.language = response.get('language', user.language)
        user.link = response.get('url', user.link)
        # Check AuthForbidden
        if not user.is_active or user.is_active != user.active:
            raise AuthForbidden('social_core.backends.google.GoogleOAuth2')
        user.save()
    return
